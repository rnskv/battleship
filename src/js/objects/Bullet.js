import AnimationSprite from '../common/AnimationSprite';
import * as helpers from '../../helpers/index'
import Store from '../common/Store';
class Bullet extends AnimationSprite {
    constructor(params) {
        super(params);
        this.dx = 0;
        this.dy = 0;
        this.speed = 50;
        this.angle = params.angle;
        this.owner = params.owner;
    }
    checkCollision(a, b) {
        return !(
            ((a.y + a.height) < (b.y)) ||
            (a.y > (b.y + b.height)) ||
            ((a.x + a.width) < b.x) ||
            (a.x > (b.x + b.width))
        )
    }
    update(delta) {
        super.update(delta);

        let point1 = {
            x: this.spritesheet.x,
            y: this.spritesheet.y
        };
        let point2 = {
            x: this.owner.spritesheet.x,
            y: this.owner.spritesheet.y,
        };
        //Math.max(window.innerWidth, window.innerHeight)
        if (helpers.getDistance(point1, point2) > Math.max(window.innerWidth, window.innerHeight)) {
            Store.removeObject(this._id);
            return
        }

        Store.state.destructibleObjects.forEach(object => {
            const isCollide = this.checkCollision(
                {
                    x: this.spritesheet.x,
                    y: this.spritesheet.y,
                    width: this.width,
                    height: this.height,
                },
                {
                    x: object.spritesheet.x,
                    y: object.spritesheet.y,
                    width: object.width,
                    height: object.height,
                },
            );
            if (isCollide) {
                Store.removeObject(this._id);
                object.health.count -= 10;
            }
        });

        this.spritesheet.visible = true;
        this.spritesheet.rotation = this.angle;
        this.dx = Math.cos(this.angle);
        this.dy = Math.sin(this.angle);
        this.x = this.x + this.dx * this.speed;
        this.y = this.y + this.dy * this.speed;
    }
}

export default Bullet;