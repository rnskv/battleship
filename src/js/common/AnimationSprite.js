import * as PIXI from "pixi.js";
import Store from './Store';
import Camera from './Camera';
class AnimationSprite {
    constructor(data) {
        this.spritesheet = new PIXI.extras.TilingSprite(data.texture);
        this.animation = data.animation;
        this.currentFrame = 0;
        this.width = data.width;
        this.height = data.height;
        this.x = data.x;
        this.y = data.y;
        this.spritesheet.x = data.x;
        this.spritesheet.y = data.y;
        this.type = 'default';

        Store.app.stage.addChild(this.spritesheet);
    }
    update(delta) {

        this.currentFrame = this.currentFrame >= this.animation.frames.length - 1 ? 0 : this.currentFrame + 1 * delta;
        let frame = this.animation.frames[Math.floor(this.currentFrame)];
        if (!frame) return;

        this.spritesheet.tilePosition.x = frame[0];
        this.spritesheet.tilePosition.y = frame[1];
        this.spritesheet.width = frame[2];
        this.spritesheet.height = frame[3];
        this.spritesheet.scale.x = this.width / this.spritesheet.width;
        this.spritesheet.scale.y = this.height / this.spritesheet.height;

        this.spritesheet.x = this.x - Camera.x;
        this.spritesheet.y = this.y - Camera.y;
    }
    remove() {
        Store.app.stage.removeChild(this.spritesheet);
    }
}

export default AnimationSprite;