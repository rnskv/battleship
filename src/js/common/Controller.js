import Camera from './Camera';

class Controller {
    constructor() {
        this.mousePosition = {
            x: 0,
            y: 0,
        };
        this.moveVector = {
            left: 0,
            right: 0,
            up: 0,
            down: 0
        };
        document.addEventListener('keydown', this.setMoveVector.bind(this));
        document.addEventListener('keyup', this.deleteMoveVector.bind(this));
        document.querySelector("canvas").addEventListener('mousemove', this.getMousePosition.bind(this));
    }
    getMousePosition(e) {
        this.mousePosition.x = e.offsetX;
        this.mousePosition.y = e.offsetY;
    }
    deleteMoveVector(e) {
        switch (e.keyCode) {
            case 87:
                this.moveVector.up = 0;
                break;
            case 83:
                this.moveVector.down = 0;
                break;
            case 65:
                this.moveVector.left = 0;
                break;
            case 68:
                this.moveVector.right = 0;
                break;
        }
    }
    setMoveVector(e) {
        switch (e.keyCode) {
            case 87:
                this.moveVector.up = 1;
                break;
            case 83:
                this.moveVector.down = 1;
                break;
            case 65:
                this.moveVector.left = 1;
                break;
            case 68:
                this.moveVector.right = 1;
                break;
        }
    }
}
export default Controller;