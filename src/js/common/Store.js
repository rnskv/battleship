import * as PIXI from "pixi.js";
import manifest from '../../manifest'
class Store {
    constructor() {
        this.app = this.createApp();
        this.resources = [];
        this.manifest = manifest;
        this.state = {
            objects: [],
            destructibleObjects: []
        }
    }
    createApp() {
        return new PIXI.Application({
            width: window.innerWidth,
            height: window.innerHeight,
            resizeTo: true,
            backgroundColor: '0x79859a'
        });
    }
    setResources(res) {
        this.resources = res;
    }
    addObject(id, object) {
        console.log('addObject', object);
        if (this.getObject(id, object.type)) throw new Error(`Объект с идентификатором ${id} уже существует`);
        object._id = id;
        switch (object.type) {
            case 'default':
                this.state.objects.push(object);
            break;
            case 'destructible':
                this.state.destructibleObjects.push(object);
            break;
        }
    }
    getObject(id) {
        const allObjects = [...this.state.objects, ...this.state.destructibleObjects];
        return allObjects.filter(object => object._id === id)[0];
    }
    removeObject(id) {
        const object = this.getObject(id);
        if (!object) return;
        switch (object.type) {
            case 'default':
                this.state.objects = this.state.objects.filter(object => object._id !== id);
                break;
            case 'destructible':
                this.state.destructibleObjects = this.state.destructibleObjects.filter(object => object._id !== id);
                break;
        }
        object.remove();
    }
}

export default new Store();